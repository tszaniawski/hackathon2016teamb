angular.module('starter.services', [])

.factory('Images', function() {
  // Might use a resource here that returns a JSON array
  // Some fake testing data
  var images = [
    {
      id:1,
      src: 'img/lifestyle/fashionista.jpg',
      scent: [1,5,3],
      phase: 1,
      selected: false
    },
    {
      id:2,
      src: 'img/lifestyle/enterpreneur.jpg',
      scent: [2,3,4],
      phase: 1,
      selected: false
    },
    {
      id:3,
      src: 'img/lifestyle/sports woman.jpg',
      scent: [1,5,3],
      phase: 1,
      selected: false
    },
    {
      id:4,
      src: 'img/lifestyle/creativity beast.jpg',
      scent: [1,5,3],
      phase: 1,
      selected: false
    },
    {
      id:5,
      src: 'img/free time/diy crafts.jpg',
      scent: [1,5,3],
      phase: 2,
      selected: false
    },
    {
      id:6,
      src: 'img/free time/sports.jpg',
      scent: [2,3,4],
      phase: 2,
      selected: false
    },
    {
      id:7,
      src: 'img/free time/music.jpg',
      scent: [1,5,3],
      phase: 2,
      selected: false
    },
    {
      id:8,
      src: 'img/free time/books.jpg',
      scent: [1,5,3],
      phase: 2,
      selected: false
    },
    {
      id:9,
      src: 'img/physical/strolling in the park.jpg',
      sub: [1,5,3],
      phase: 3,
      selected: false
    },
    {
      id:10,
      src: 'img/physical/fitness.jpg',
      sub: [2,3,4],
      phase: 3,
      selected: false
    },
    {
      id:11,
      src: 'img/physical/crossfit.jpg',
      sub: [1,5,3],
      phase: 3,
      selected: false
    },
    {
      id:12,
      src: 'img/physical/swimming.jpg',
      sub: [1,5,3],
      phase: 3,
      selected: false
    },
    {
      id:13,
      src: 'img/flavours/bitter.jpg',
      sub: [1,5,3],
      phase: 4,
      selected: false
    },
    {
      id:14,
      src: 'img/flavours/sour.jpg',
      sub: [2,3,4],
      phase: 4,
      selected: false
    },
    {
      id:15,
      src: 'img/flavours/sweet.jpg',
      sub: [1,5,3],
      phase: 4,
      selected: false
    },
    {
      id:16,
      src: 'img/flavours/savoury.jpg',
      sub: [1,5,3],
      phase: 4,
      selected: false
    }


  ];

  return {
    getAll: function() {
      return images;
    },
    getByPhase: function (phase) {
      var phaseImages = [];
      for (var i = 0; i < images.length; i++) {
        if (images[i].phase == phase) {
          phaseImages.push(images[i]);
        }
      }
      return phaseImages;
    },
    remove: function(img) {
      chats.splice(chats.indexOf(img), 1);
    },
    get: function(imgId) {
      for (var i = 0; i < images.length; i++) {
        if (images[i].id === parseInt(imgId)) {
          return images[i];
        }
      }
      return null;
    }
  };
})


.factory('Chats', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var chats = [{
    id: 0,
    name: 'Ben Sparrow',
    lastText: 'You on your way?',
    face: 'img/ben.png'
  }, {
    id: 1,
    name: 'Max Lynx',
    lastText: 'Hey, it\'s me',
    face: 'img/max.png'
  }, {
    id: 2,
    name: 'Adam Bradleyson',
    lastText: 'I should buy a boat',
    face: 'img/adam.jpg'
  }, {
    id: 3,
    name: 'Perry Governor',
    lastText: 'Look at my mukluks!',
    face: 'img/perry.png'
  }, {
    id: 4,
    name: 'Mike Harrington',
    lastText: 'This is wicked good ice cream.',
    face: 'img/mike.png'
  }];

  return {
    all: function() {
      return chats;
    },
    remove: function(chat) {
      chats.splice(chats.indexOf(chat), 1);
    },
    get: function(chatId) {
      for (var i = 0; i < chats.length; i++) {
        if (chats[i].id === parseInt(chatId)) {
          return chats[i];
        }
      }
      return null;
    }
  };
});
