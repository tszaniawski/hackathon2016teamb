angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope, Images) {
  $scope.items = Images.getByPhase("1");
  $scope.addToSelected = function (item) {

  item.selected = !item.selected;
    if(localStorage) {
      if (!localStorage.getItem("selected")) {
        localStorage.setItem("selected",  JSON.stringify([]));
      }
      var selected = JSON.parse(localStorage.getItem("selected"));
      selected.push(item);
      localStorage.setItem("selected", JSON.stringify(selected));
    }
  };
})

.controller('Select2Ctrl', function($scope, Images) {
  $scope.items = Images.getByPhase("2");
  $scope.addToSelected = function (item) {
    item.selected = !item.selected;
    if(localStorage) {
      if (!localStorage.getItem("selected")) {
        localStorage.setItem("selected",  JSON.stringify([]));
      }
      var selected = JSON.parse(localStorage.getItem("selected"));
      selected.push(item);
      localStorage.setItem("selected", JSON.stringify(selected));
    }
  };
})

.controller('Select3Ctrl', function($scope, Images) {
  $scope.items = Images.getByPhase("3");
  $scope.addToSelected = function (item) {
    item.selected = !item.selected;
    if(localStorage) {
      if (!localStorage.getItem("selected")) {
        localStorage.setItem("selected",  JSON.stringify([]));
      }
      var selected = JSON.parse(localStorage.getItem("selected"));
      selected.push(item);
      localStorage.setItem("selected", JSON.stringify(selected));
    }
  };
})

.controller('Select4Ctrl', function($scope, Images) {
  $scope.items = Images.getByPhase("4");
  $scope.addToSelected = function (item) {
    item.selected = !item.selected;
    if(localStorage) {
      if (!localStorage.getItem("selected")) {
        localStorage.setItem("selected",  JSON.stringify([]));
      }
      var selected = JSON.parse(localStorage.getItem("selected"));
      selected.push(item);
      localStorage.setItem("selected", JSON.stringify(selected));
    }
  };
})

.controller('SolutionCtrl', function($scope, Images) {


})
.controller('ChatsCtrl', function($scope, Chats) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
});
